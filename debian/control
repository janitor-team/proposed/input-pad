Source: input-pad
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Asias He <asias.hejun@gmail.com>,
 Osamu Aoki <osamu@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-gir,
 intltool (>= 0.40.0),
 libgirepository1.0-dev,
 libgtk-3-dev,
 libtool,
 libxkbfile-dev,
 libxklavier-dev (>= 4.0),
 libxml2-dev (>= 2.0),
 libxtst-dev,
 pkg-config,
 swig,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://github.com/fujiwarat/input-pad/wiki
Vcs-Git: https://salsa.debian.org/input-method-team/input-pad.git
Vcs-Browser: https://salsa.debian.org/input-method-team/input-pad

Package: gir1.2-inputpad-1.1
Section: introspection
Architecture: any
Breaks: gir1.2-input-pad-1.0 (<< 1.0.3-4~exp2)
Replaces: gir1.2-input-pad-1.0 (<< 1.0.3-4~exp2)
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: On-screen Input Pad to Send Characters with Mouse - introspection data
 The input pad is a tool to send a character to text applications when the
 corresponging button is pressed. It provides the GTK+ based GUI and can send
 characters when the GTK+ buttons are pressed.
 .
 This package contains the GObject introspection data of input-pad.

Package: input-pad
Architecture: any
Depends:
 libinput-pad-1.0-1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: On-screen Input Pad to Send Characters with Mouse
 The input pad is a tool to send a character to text applications when the
 corresponging button is pressed. It provides the GTK+ based GUI and can send
 characters when the GTK+ buttons are pressed.
 .
 This package contains a standalone application.

Package: libinput-pad-dev
Section: libdevel
Architecture: any
Depends:
 libglib2.0-dev,
 libgtk-3-dev,
 libinput-pad-1.0-1 (= ${binary:Version}),
 gir1.2-inputpad-1.1 (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 libinput-pad-xtest,
Provides:
 libinput-pad-dev,
Description: On-screen Input Pad to Send Characters with Mouse - dev
 The input pad is a tool to send a character to text applications when the
 corresponging button is pressed. It provides the GTK+ based GUI and can send
 characters when the GTK+ buttons are pressed.
 .
 This package contains the header files.

Package: libinput-pad-xtest
Section: libs
Architecture: any
Depends:
 libinput-pad-1.0-1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: On-screen Input Pad to Send Characters with Mouse - xtest
 The input pad is a tool to send a character to text applications when the
 corresponging button is pressed. It provides the GTK+ based GUI and can send
 characters when the GTK+ buttons are pressed.
 .
 This package contains the XTEST module for input-pad.

Package: libinput-pad-1.0-1
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 libinput-pad0,
 libinput-pad1,
Conflicts:
 libinput-pad0,
 libinput-pad1,
Enhances:
 libinput-pad-xtest,
Description: On-screen Input Pad to Send Characters with Mouse - libs
 The input pad is a tool to send a character to text applications when the
 corresponging button is pressed. It provides the GTK+ based GUI and can send
 characters when the GTK+ buttons are pressed.
 .
 This package contains libraries for other applications.
